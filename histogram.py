import random as rand
import threading as thrd
import matplotlib.pyplot as plt
wynik = {}

def obliczanie(poczatek, koniec, lock, dane):
    lock.acquire()
    temp = {}
    for x in dane[poczatek:koniec]:
        if not int(x) in temp:
            temp[int(x)] = 1
        else:
            temp[int(x)] += 1
            
    for key, value in temp.items():
        if key in wynik:
            wynik[key] += value
        else:
            wynik[key] = value
            
    lock.release()

if __name__ == "__main__":
    dane = []
    for i in range(1000):
        dane.append(rand.random()*100)
    thrd_arr = []
    lock = thrd.Lock()
    for i in range(4):
        t = thrd.Thread(target=obliczanie, args=(0 + i*250, 250 + i*250, lock, dane))
        thrd_arr.append(t)
        t.start()
    for thread in thrd_arr:
        thread.join()
    wynik = dict(sorted(wynik.items()))
    out_list = [wynik[k] for k in wynik]
    plt.stem(out_list)
    plt.show()
    plt.hist(dane,100)
    plt.show()