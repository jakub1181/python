class FunkcjaKwadratowa:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
    def Rozwiaz(self):
        wynik = []
        if self.a ==0:
            wynik.append(-self.c/self.b)
        else:

            delta = (self.b)**2 - 4*self.a*self.c
            if delta > 0:

                wynik.append((-self.b-(delta)**0.5)/(2*self.a))
                wynik.append((-self.b+(delta)**0.5)/(2*self.a))
            elif delta == 0:
                wynik.append(-self.b/(2*self.a))
            else:
                print("Nieskonczenie wiele rozwiazan")
        return wynik

p1 = FunkcjaKwadratowa(1,-6,5)
print(p1.Rozwiaz())



