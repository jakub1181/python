import os

def film():
    print("Wprowadz informacje o filmie.")
    numer = 0
    baza = []
    baza.append([])
    while True:
        dane = input("Numer {}. Podaj tytul filmu:\n".format(numer))
        if dane == "":
            break
        baza[numer].append(dane)
        dane = input("Numer {}. Kto byl rezyserem?\n".format(numer))
        baza[numer].append(dane)
        dane = input("Numer {}. Ile gwiazdek dalbys temu filmowi w skali 1-10?\n".format(numer))
        baza[numer].append(dane)
        numer +=1
        baza.append([])
    filmy = ''
    for i in range(len(baza)-1):
        filmy += "Numer" + str(i) + " " + baza[i][0] + ',' + baza[i][1] + ',' + baza[i][2]
        if(i != len(baza)-2):
            filmy +="\n"
    return filmy
    

def baza_filmow():
    fileslist = os.listdir(os.getcwd())
    if("csv_json.txt" not in fileslist):
        nowy_film = film()
        f = open("csv_json.txt", "w+", encoding='utf-8')
        f.write(nowy_film)
        f.close()
        return
    
    f = open("csv_json.txt", "r+", encoding='utf-8')
    text = f.read()
    f.close()
    list_of_lines = text.split("\n")
    print(text)

    while True:
        print("Ktory film chcesz usunac? Podaj numer\n")
        do_usuniecia = input()
        if (do_usuniecia != ""):
            do_usuniecia = int(do_usuniecia)
            list_of_lines.pop(do_usuniecia)
            test = "\n".join(list_of_lines)
            print(test)
            f = open("csv_json.txt", "w+", encoding='utf-8')
            f.write(test)
            f.close()
            if(len(list_of_lines) == 0):
                break
        else:
            break
    f = open("csv_json.txt", "r+", encoding='utf-8')
    text = f.read()
    f.close()
    nowy_film = film()
    if(text == ""):
        nowy_film += text
    else:
        if(nowy_film != ""):
            nowy_film += text + "\n"
        else:
            nowy_film = text
    f = open("csv_json.txt", "w+", encoding='utf-8')
    f.write(nowy_film)
    f.close()

baza_filmow()
