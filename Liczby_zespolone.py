class Zespolone:
    def __init__(self, real, imag):
        self.real = real
        self.imag = imag

    def wyswietlanie(self):
        if self.imag <0:
            print("{}{}i".format(self.real, self.imag))
        else:
            print("{}+{}i".format(self.real, self.imag))


    @staticmethod
    def dodawanie(z1,z2):
        print("Suma: ")
        return Zespolone(z1.real + z2.real, z1.imag + z2.imag)
    
    @staticmethod
    def odejmowanie(z1,z2):
        print("Roznica: ")
        return Zespolone(z1.real - z2.real, z1.imag - z2.imag)

    @staticmethod
    def mnozenie(z1,z2):
        a = z1.real
        b = z1.imag
        c = z2.real
        d = z2.imag
        print("Iloczyn: ")
        return Zespolone(a*c - b*d, b*c + a*d)

    @staticmethod
    def dzielenie(z1,z2):
        a = z1.real
        b = z1.imag
        c = z2.real
        d = z2.imag
        licznik_real = a*c + b*d
        licznik_imag = b*c + a*d
        mianownik = z2.real**2 + z2.imag**2
        print("Iloraz: ")
        return Zespolone(licznik_real/mianownik, licznik_imag/mianownik)


z1 = Zespolone(3,4)
z2 = Zespolone(-1,2)
Zespolone.dodawanie(z1,z2).wyswietlanie()
Zespolone.odejmowanie(z1,z2).wyswietlanie()
Zespolone.mnozenie(z1,z2).wyswietlanie()
Zespolone.dzielenie(z1,z2).wyswietlanie()
