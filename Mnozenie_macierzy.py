import numpy as np

x = np.random.randint(1,100, size=(8,8))
y = np.random.randint(1,100, size=(8,8))

z = np.zeros((8,8))

for i in range(len(x)):
    for j in range(len(y[0])):
        wynik = 0
        for k in range(len(x[0])):
            wynik += x[i][k] * y[k][j]
            z[i][j] = wynik

print(z)